<?php return [
  'plugin' => [
      'name' => 'Fallback loclae',
      'description' => 'Provides a fallback locale.',
  ],
  'list' => [
    'is_fallback' => 'Fallback',
  ],
  'form' => [
    'is_fallback' => 'Fallback',
    'is_fallback_help' => 'The fallback language will be used if no matching language was found.'
  ],
  'locale' => [
    'unset_fallback' => '":locale" is already fallback and cannot be unset as fallback.',
    'delete_fallback' => '":locale" is the fallback and cannot be deleted.',
    'disabled_fallback' => '":locale" is disabled and cannot be set as fallback.',
  ],
];