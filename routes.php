<?php
/**
 * Created by PhpStorm.
 * User: r
 * Date: 13.04.16
 * Time: 16:12
 */

use RainLab\Translate\Classes\Translator;
use RainLab\Translate\Models\Locale;

App::before(function($request) {

    if (App::runningInBackend()) {
        return;
    }

    $translator = Translator::instance();
    if (!$translator->isConfigured())
        return;

    $fallbackLocale = Locale::getFallback();
    
    if ($fallbackLocale) {
        $translator->setLocale($fallbackLocale->code);
    }
});