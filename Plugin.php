<?php namespace StudioBosco\FallbackLocale;

use Lang;
use Cache;
use ApplicationException;
use ValidationException;
use System\Classes\PluginBase;
use RainLab\Translate\Models\Locale;
use RainLab\Translate\Controllers\Locales;

/**
 * Theme Plugin Information File
 */
class Plugin extends PluginBase
{
    public static $fallbackLocale = null;

    /**
     * @var array Plugin dependencies
     */
    public $require = [
        'RainLab.Translate',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'studiobosco.fallbacklocale::lang.plugin.name',
            'description' => 'studiobosco.fallbacklocale::lang.plugin.description',
            'author'      => 'Studio Bosco',
            'icon'        => 'icon-language',
            'homepage'    => 'https://gitlab.com/studiobosco/octobercms/fallbacklocale',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Locales::extendListColumns(function ($list, $model) {
            if (!$model instanceof Locale) {
                return;
            }

            $list->addColumns([
                'studiobosco_fallbacklocale_is_fallback' => [
                    'label' => 'studiobosco.fallbacklocale::lang.list.is_fallback',
                    'type' => 'switch',
                ],
            ]);
        });

        Locales::extendFormFields(function ($form, $model) {
            if (!$model instanceof Locale) {
                return;
            }

            $form->addFields([
                'studiobosco_fallbacklocale_is_fallback' => [
                    'label' => 'studiobosco.fallbacklocale::lang.form.is_fallback',
                    'type' => 'checkbox',
                    'comment' => 'studiobosco.fallbacklocale::lang.form.is_fallback_help',
                ],
            ]);
        });

        Locale::extend(function ($model) {
            $model->addDynamicMethod('makeFallback', function () use ($model) {
                if (!$model->is_enabled) {
                    throw new ValidationException(['is_enabled' => Lang::get('studiobosco.fallbacklocale::lang.locale.disabled_fallback', ['locale' => $model->name])]);
                }

                Plugin::$fallbackLocale = $model->code;
                Cache::forget('studiobosco.fallbacklocale.fallbackLocale');

                $model->newQuery()->where('id', $model->id)->update(['studiobosco_fallbacklocale_is_fallback' => true]);
                $model->newQuery()->where('id', '<>', $model->id)->update(['studiobosco_fallbacklocale_is_fallback' => false]);
            });
            $model->addDynamicMethod('getFallback', function() use ($model) {
                if (Plugin::$fallbackLocale !== null) {
                    return Plugin::$fallbackLocale;
                }

                return Plugin::$fallbackLocale = Locale::where('studiobosco_fallbacklocale_is_fallback', true)
                ->remember(1440, 'studiobosco.fallbacklocale.fallbackLocale')
                ->first();
            });

            $model->bindEvent('model.beforeUpdate', function() use ($model) {
                if ($model->isDirty('studiobosco_fallbacklocale_is_fallback')) {
                    $model->makeFallback();

                    if (!$model->studiobosco_fallbacklocale_is_fallback) {
                        throw new ValidationException(['is_fallback' => Lang::get('studiobosco.fallbacklocale::lang.locale.unset_fallback', ['locale' => $model->name])]);
                    }
                }
            });
            $model->bindEvent('model.afterCreate', function() use ($model) {
                if ($model->studiobosco_fallbacklocale_is_fallback) {
                    $model->makeFallback();
                }
            });
            $model->bindEvent('model.beforeDelete', function() use ($model) {
                if ($model->is_default) {
                    throw new ApplicationException(Lang::get('studiobosco.fallbacklocale::lang.locale.delete_fallback', ['locale' => $model->name]));
                }
            });
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [];
    }
}
