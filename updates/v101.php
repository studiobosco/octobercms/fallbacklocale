<?php

namespace StudioBosco\FallbackLocale\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class V101 extends Migration
{

  public function up()
  {
    Schema::table('rainlab_translate_locales', function ($table) {
      $table->boolean('studiobosco_fallbacklocale_is_fallback')->default(false);
    });
  }

  public function down()
  {
    Schema::table('rainlab_translate_locales', function ($table) {
      $table->dropColumn('studiobosco_fallbacklocale_is_fallback');
    });
  }
}
