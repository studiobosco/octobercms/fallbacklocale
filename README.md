# Fallback locale plugin

This plugin provides a fallback locale next to a default locale.

The default locale is still used as the default when editing content, while the fallback locale is used as a fallback in the frontend if no matching locale was found.